import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';

/**
 * Generated class for the CourseCardComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'lesson-card',
  templateUrl: 'lesson-card.html'
})
export class LessonCardComponent {

  @Input('imgSrc') imgSrc : String;
  @Input('course') title : String;
  
  constructor(public navCtrl: NavController) {
   
  }

  tapEvent($event)
  {
    
  }

}
