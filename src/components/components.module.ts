import { NgModule } from '@angular/core';
import { ProfileCourseCardComponent } from './profile-course-card/profile-course-card';
@NgModule({
	declarations: [ProfileCourseCardComponent],
	imports: [],
	exports: [ProfileCourseCardComponent]
})
export class ComponentsModule {}
