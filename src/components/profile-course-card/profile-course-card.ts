import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';

import { SingleCoursePage } from '../../pages/single-course/single-course';

/**
 * Generated class for the ProfileCourseCardComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'profile-course-card',
  templateUrl: 'profile-course-card.html'
})
export class ProfileCourseCardComponent {

  @Input('percentage') percentage : string;
  @Input('course') title : string;
  
  text: string;
  completed: boolean = false;


  constructor(public navCtrl: NavController) {
    console.log('Hello ProfileCourseCardComponent Component');
    this.text = 'Hello World';
  }

  ngOnInit()
  {
    var value = parseFloat(this.percentage);
    if(value == 100.0){
      this.completed = true;
    }
    else{
      this.completed = false;
    }
  }

  tapEvent($event)
  {
    this.navCtrl.push(SingleCoursePage, {course: this.title});
  }


}
