import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';

import { SingleCoursePage } from '../../pages/single-course/single-course';

/**
 * Generated class for the CourseCardComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'course-card',
  templateUrl: 'course-card.html'
})
export class CourseCardComponent {

  @Input('imgSrc') imgSrc : String;
  @Input('course') title : String;
  @Input('isFollowing') isFollowing : boolean = true;

  
  constructor(public navCtrl: NavController) {
   
  }

  tapEvent($event)
  {
    this.navCtrl.push(SingleCoursePage, {course: this.title});
  }

}
