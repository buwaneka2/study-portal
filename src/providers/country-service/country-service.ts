import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

let apiUrl = 'http://dev.alibabalearn.com/api/v1/countries';

/*
  Generated class for the CountryServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CountryServiceProvider {
  results;

  constructor(public http: Http) {
    console.log('Hello CountryServiceProvider Provider');
  }

  getCountry(){
    let promise =  new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');

      this.http.get(apiUrl)
        .toPromise()
        .then(res => {
          this.results = this.generateArray(res.json().data);
          resolve();
        }, 
        (err) => {
          reject(err);
        });
    });
    return promise;
  }

  generateArray(obj){
    return Object.keys(obj).map((key)=>{ return {id: key, country: obj[key]}});
 }
}

class Country{
  id: number;
  country: String;
}
