import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'lesson',
  templateUrl: 'lesson.html'
})
export class LessonPage {

  constructor(public navCtrl: NavController) {

  }

}
