import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import  { ProfileCourseCardComponent } from '../../components/profile-course-card/profile-course-card';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {

  constructor(public navCtrl: NavController) {

  }

}
