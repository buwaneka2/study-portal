import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'single-course',
  templateUrl: 'single-course.html'
})
export class SingleCoursePage {

    courseDescription: String;
    topic: String;

  constructor(public navCtrl: NavController,  private navParams: NavParams) {
      this.courseDescription = "overflow-wrap. The overflow-wrap CSS property specifies whether or not the browser should insert line breaks within words to prevent text from overflowing its content box. Note: In contrast to word-break , overflow-wrap will only create a break if an entire word cannot be placed on its own line without overflowing.";
      this.topic = navParams.get('course');
  }

}
