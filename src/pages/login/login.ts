import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { NavController } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { TabsPage } from '../tabs/tabs';

import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

@Component({
  selector: 'login',
  templateUrl: 'login.html'
})
export class LoginPage {


  responseData: any;
  userData: FormGroup;
  loginFail: boolean;
  username: AbstractControl;
  password: AbstractControl;

  constructor(public navCtrl: NavController, public authService:AuthServiceProvider, private formBuilder: FormBuilder) {
    this.userData = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
    });
    this.loginFail = false;
    this.username = this.userData.controls['email'];
    this.password = this.userData.controls['password']
  }
   
  logIn() {
    this.loginFail = false;
   this.authService.postData(this.userData.value, 'login').then((result) => {
     this.responseData = result;
     localStorage.setItem('userData', JSON.stringify(this.responseData));
     this.navCtrl.push(TabsPage);
   }, (err) => {
     this.loginFail = true;
   });
  }

  signUp(){
    this.navCtrl.push(RegisterPage);
  }

}
