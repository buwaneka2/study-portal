import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { SingleCoursePage } from '../single-course/single-course';

@Component({
  selector: 'courses',
  templateUrl: 'courses.html'
})
export class CoursesPage {

    allCourses: any[];
    myCourses: any[];
    courseType: String;
    isAll: boolean;

  constructor(public navCtrl: NavController) {
      this.courseType = "allCourses";
      this.isAll = true;
  }

  updateCourseData($event) {
      if(this.courseType == "allCourses"){
          this.isAll = true;
      }
      else{
          this.isAll = false;
      }
  }



}
