import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { CountryServiceProvider } from '../../providers/country-service/country-service';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

import { TabsPage } from '../tabs/tabs';

import 'rxjs/add/operator/catch';

@Component({
  selector: 'register',
  templateUrl: 'register.html'
})
export class RegisterPage {

  userData: FormGroup;
  responseData: any;
  values: any;
  loading: boolean;
  signUpFail: boolean = false;
 
  constructor(public navCtrl: NavController, public countryService:CountryServiceProvider, public authService: AuthServiceProvider, private formBuilder: FormBuilder) {
    this.userData = this.formBuilder.group({
      first_name: ['', Validators.compose([Validators.required])],
      last_name:['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      confirm_password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      country: ['144', Validators.required]
    });
  }

  signUp() {
    this.signUpFail = false;
    this.authService.postData(this.userData.value, 'register').then((result) => {
      this.responseData = result;
      localStorage.setItem('userData', JSON.stringify(this.responseData));
      this.navCtrl.push(TabsPage);
    }, (err) => {
      this.signUpFail = true;
    });
  }

  ngOnInit() {
    this.loading = true;
    this.countryService.getCountry().then(() => {this.loading = false});
  }

}