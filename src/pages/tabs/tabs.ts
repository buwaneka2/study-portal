import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { CoursesPage } from '../courses/courses';
import { HomePage } from '../home/home';
import { ProfilePage } from '../profile/profile';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = CoursesPage;
  tab3Root = ProfilePage;

  constructor() {

  }
}
