import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { ProfilePage } from '../pages/profile/profile';
import { CoursesPage } from '../pages/courses/courses';
import { SingleCoursePage } from '../pages/single-course/single-course';
import { RegisterPage } from '../pages/register/register';
import { LoginPage } from '../pages/login/login';

import { CourseCardComponent } from '../components/course-card/course-card';
import { LessonCardComponent } from '../components/lesson-card/lesson-card';
import { ProfileCourseCardComponent } from '../components/profile-course-card/profile-course-card';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { BackgroundProvider } from '../providers/background/background';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { CountryServiceProvider } from '../providers/country-service/country-service';

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';


@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    ProfilePage,
    TabsPage,
    CoursesPage,
    CourseCardComponent,
    SingleCoursePage,
    LessonCardComponent,
    ProfileCourseCardComponent,
    RegisterPage,
    LoginPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    IonicStorageModule.forRoot(),
    Ionic2RatingModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    ProfilePage,
    TabsPage,
    CoursesPage,
    SingleCoursePage,
    RegisterPage,
    LoginPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    BackgroundProvider,
    AuthServiceProvider,
    CountryServiceProvider,
  ]
})
export class AppModule {}
